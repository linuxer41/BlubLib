using System;
using System.IO;
using BlubLib.Reflection;
using Sigil;

namespace BlubLib.Serialization.Tests.Serializers
{
    public class Int32AsByteSerializer : ISerializer<int>
    {
        public bool CanHandle(Type type)
        {
            return type.GetTypeCode() == TypeCode.Int32;
        }

        public void Serialize(BlubSerializer blubSerializer, BinaryWriter writer, int value)
        {
            writer.Write((byte)value);
        }

        public int Deserialize(BlubSerializer blubSerializer, BinaryReader reader)
        {
            return reader.ReadByte();
        }
    }

    public class Int32AsByteCompiler : ISerializerCompiler
    {
        public bool CanHandle(Type type)
        {
            return type.GetTypeCode() == TypeCode.Int32;
        }

        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            ctx.Emit.LoadReaderOrWriterParam();
            ctx.Emit.LoadLocal(value);
            ctx.Emit.CallVirtual(ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(byte))));
        }

        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            ctx.Emit.LoadReaderOrWriterParam();
            ctx.Emit.CallVirtual(ReflectionHelper.GetMethod((BinaryReader _) => _.ReadByte()));
            ctx.Emit.StoreLocal(value);
        }
    }
}
