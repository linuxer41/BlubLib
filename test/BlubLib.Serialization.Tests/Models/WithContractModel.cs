﻿namespace BlubLib.Serialization.Tests.Models
{
    [BlubContract]
    public class WithContractModel
    {
        [BlubMember]
        public byte A { get; set; }

        public byte B { get; set; }

        [BlubMember]
        public byte C { get; set; }

        [BlubIgnore]
        public byte D { get; set; }
    }
}
