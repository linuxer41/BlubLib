﻿using BlubLib.Serialization.Serializers;

namespace BlubLib.Serialization.Tests.Models
{
    [BlubContract]
    public class CStringModel
    {
        [BlubMember(0)]
        [BlubSerializer(typeof(CStringSerializer))]
        public string Value { get; set; }

        [BlubMember(1)]
        [BlubSerializer(typeof(CStringSerializer), 5)]
        public string Value2 { get; set; }

        public CStringModel()
        {
        }

        public CStringModel(string value, string value2)
        {
            Value = value;
            Value2 = value2;
        }
    }
}
