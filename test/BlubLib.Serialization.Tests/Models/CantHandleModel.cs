using BlubLib.Serialization.Tests.Serializers;

namespace BlubLib.Serialization.Tests.Models
{
    [BlubSerializer(typeof(CantHandleSerializer))]
    public class CantHandleModel
    {
    }

    [BlubContract]
    public class CantHandleOnMemberModel
    {
        [BlubMember(0)]
        [BlubSerializer(typeof(CantHandleSerializer))]
        public int A { get; set; }
    }
}
