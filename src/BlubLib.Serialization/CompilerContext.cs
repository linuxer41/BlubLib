﻿using Sigil.NonGeneric;

namespace BlubLib.Serialization
{
    /// <summary>
    /// The compiler context containing the <see cref="BlubSerializer"/> and <see cref="Emit"/> instance
    /// </summary>
    public class CompilerContext
    {
        /// <summary>
        /// Gets the <see cref="BlubSerializer"/>
        /// </summary>
        public BlubSerializer BlubSerializer { get; }

        /// <summary>
        /// Gets the IL code emiter
        /// </summary>
        public Emit Emit { get; }

        internal SerializerGenerator Generator { get; }

        internal CompilerContext(BlubSerializer blubSerializer, Emit emit, SerializerGenerator generator)
        {
            BlubSerializer = blubSerializer;
            Emit = emit;
            Generator = generator;
        }
    }
}
