﻿using System;
using System.IO;
using System.Reflection;
using BlubLib.IO;
using BlubLib.Reflection;
using Sigil;

namespace BlubLib.Serialization.Serializers
{
    /// <summary>
    /// Serializer for <see cref="string"/> using <see cref="BinaryWriterExtensions.WriteCString(BinaryWriter, string)"/>/<see cref="BinaryReaderExtensions.ReadCString(BinaryReader)"/>
    /// </summary>
    public class CStringSerializer : ISerializerCompiler
    {
        // ReSharper disable InvokeAsExtensionMethod
        private static readonly MethodInfo s_writeMethod = ReflectionHelper.GetMethod(() => BinaryWriterExtensions.WriteCString(default(BinaryWriter), default(string)));
        private static readonly MethodInfo s_writeMethod2 = ReflectionHelper.GetMethod(() => BinaryWriterExtensions.WriteCString(default(BinaryWriter), default(string), default(int)));
        private static readonly MethodInfo s_readMethod = ReflectionHelper.GetMethod(() => BinaryReaderExtensions.ReadCString(default(BinaryReader)));
        private static readonly MethodInfo s_readMethod2 = ReflectionHelper.GetMethod(() => BinaryReaderExtensions.ReadCString(default(BinaryReader), default(int)));
        // ReSharper restore InvokeAsExtensionMethod

        private readonly int _length;

        /// <summary>
        /// Initializes a new instance of the <see cref="CStringSerializer"/> class.
        /// </summary>
        public CStringSerializer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CStringSerializer"/> class.
        /// </summary>
        /// <param name="length">The length of the array. Must be greater than 0</param>
        public CStringSerializer(int length)
        {
            if (length < 1)
                throw new ArgumentOutOfRangeException(nameof(length));

            _length = length;
        }

        /// <inheritdoc/>
        public bool CanHandle(Type type)
        {
            return type == typeof(string);
        }

        /// <inheritdoc/>
        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            ctx.Emit.LoadReaderOrWriterParam();
            ctx.Emit.LoadLocal(value);

            if (_length > 0)
            {
                ctx.Emit.LoadConstant(_length);
                ctx.Emit.Call(s_writeMethod2);
            }
            else
            {
                ctx.Emit.Call(s_writeMethod);
            }
        }

        /// <inheritdoc/>
        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            ctx.Emit.LoadReaderOrWriterParam();

            if (_length > 0)
            {
                ctx.Emit.LoadConstant(_length);
                ctx.Emit.Call(s_readMethod2);
            }
            else
            {
                ctx.Emit.Call(s_readMethod);
            }

            ctx.Emit.StoreLocal(value);
        }
    }
}
