﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using BlubLib.Reflection;
using Sigil;

namespace BlubLib.Serialization.Serializers
{
    internal class PrimitiveSerializer : ISerializerCompiler
    {
        private static readonly IReadOnlyDictionary<Type, (MethodInfo read, MethodInfo write)> s_primitiveMethods;
        private readonly Type _primitivetype;
        private readonly MethodInfo _writeMethod;
        private readonly MethodInfo _readMethod;

        static PrimitiveSerializer()
        {
            s_primitiveMethods = new Dictionary<Type, (MethodInfo read, MethodInfo write)>
            {
                [typeof(ulong)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadUInt64()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(ulong)))
                ),
                [typeof(uint)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadUInt32()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(uint)))
                ),
                [typeof(ushort)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadUInt16()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(ushort)))
                ),
                [typeof(byte)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadByte()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(byte)))
                ),

                [typeof(long)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadInt64()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(long)))
                ),
                [typeof(int)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadInt32()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(int)))
                ),
                [typeof(short)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadInt16()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(short)))
                ),
                [typeof(sbyte)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadSByte()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(sbyte)))
                ),
                [typeof(decimal)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadDecimal()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(decimal)))
                ),
                [typeof(double)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadDouble()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(double)))
                ),
                [typeof(float)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadSingle()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(float)))
                ),
                [typeof(char)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadChar()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(char)))
                ),
                [typeof(bool)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadBoolean()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(bool)))
                ),
                [typeof(string)] =
                (
                ReflectionHelper.GetMethod((BinaryReader _) => _.ReadString()),
                ReflectionHelper.GetMethod((BinaryWriter _) => _.Write(default(string)))
                )
            };
        }

        public PrimitiveSerializer(Type primitiveType)
        {
            if (!s_primitiveMethods.TryGetValue(primitiveType, out var methods))
                throw new ArgumentException("No Read/Write methods found for the given type", nameof(primitiveType));

            _primitivetype = primitiveType;
            _writeMethod = methods.write;
            _readMethod = methods.read;
        }

        public bool CanHandle(Type type)
        {
            return type == _primitivetype;
        }

        public void EmitSerialize(CompilerContext ctx, Local value)
        {
            ctx.Emit.LoadReaderOrWriterParam();
            ctx.Emit.LoadLocal(value);
            ctx.Emit.CallVirtual(_writeMethod);
        }

        public void EmitDeserialize(CompilerContext ctx, Local value)
        {
            ctx.Emit.LoadReaderOrWriterParam();
            ctx.Emit.CallVirtual(_readMethod);
            ctx.Emit.StoreLocal(value);
        }
    }
}
