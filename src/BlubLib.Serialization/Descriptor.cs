﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using BlubLib.Collections.Concurrent;

namespace BlubLib.Serialization
{
    internal class TypeModel
    {
        private readonly BlubSerializer _serializer;
        private readonly ConcurrentDictionary<Type, Descriptor> _descriptors = new ConcurrentDictionary<Type, Descriptor>();

        public TypeModel(BlubSerializer serializer)
        {
            _serializer = serializer;
        }

        public Descriptor GetDescriptor(Type type)
        {
            return _descriptors.GetValueOrDefault(type);
        }

        public Descriptor GetOrCreateDescriptor(Type type)
        {
            var descriptor = GetDescriptor(type) ?? CreateDescriptor(type);
            _descriptors.TryAdd(type, descriptor);
            return descriptor;
        }

        private Descriptor CreateDescriptor(Type type)
        {
            Descriptor parent = null;

            // Ignore Object, ValueType, Enum and Array parent types
            if (type.BaseType != typeof(object) &&
                type.BaseType != typeof(ValueType) &&
                type.BaseType != typeof(Enum) &&
                type.BaseType != typeof(Array))
            {
                parent = GetOrCreateDescriptor(type.BaseType);
            }

            var contractAttribute = type.GetCustomAttribute<BlubContractAttribute>();
            var serializerAttribute = type.GetCustomAttribute<BlubSerializerAttribute>();
            var descriptor = new Descriptor { Type = type, Parent = parent };

            if (serializerAttribute != null)
            {
                // Serializer was provided by the user

                if (typeof(ISerializer).IsAssignableFrom(serializerAttribute.SerializerType))
                {
                    descriptor.Serializer = (ISerializer)Activator.CreateInstance(
                        serializerAttribute.SerializerType,
                        serializerAttribute.SerializerParameters
                    );

                    if (!descriptor.Serializer.CanHandle(type))
                    {
                        throw new SerializerException(
                            $"Serializer '{serializerAttribute.SerializerType.FullName}' can not handle '{type.FullName}'"
                        );
                    }

                    return descriptor;
                }

                if (typeof(ISerializerCompiler).IsAssignableFrom(serializerAttribute.SerializerType))
                {
                    descriptor.Compiler = (ISerializerCompiler)Activator.CreateInstance(
                        serializerAttribute.SerializerType,
                        serializerAttribute.SerializerParameters
                    );

                    if (!descriptor.Compiler.CanHandle(type))
                    {
                        throw new SerializerException(
                            $"Serializer '{serializerAttribute.SerializerType.FullName}' can not handle '{type.FullName}'"
                        );
                    }

                    return descriptor;
                }

                throw new SerializerException(
                    $"Serializer '{serializerAttribute.SerializerType.FullName}' assigned to '{type.FullName}' has to implement {nameof(ISerializer)} or {nameof(ISerializerCompiler)}"
                );
            }

            // Check if there is a serializer available
            // Lookup order: Compiler > Serializer > Primitive > Try to figure it out
            descriptor.Compiler = _serializer.GetCompilerForType(type);
            if (descriptor.Compiler == null)
            {
                descriptor.Serializer = _serializer.GetSerializerForType(type);
                if (descriptor.Serializer == null)
                    descriptor.Compiler = _serializer.GetPrimitiveForType(type);
            }

            // We already have a serializer - No need to check members
            if (descriptor.Serializer != null || descriptor.Compiler != null)
                return descriptor;

            // No serializer was provided
            // Go through all members and try to find the needed serializers for each member

            var typeInfo = type.GetTypeInfo();
            descriptor.Members = new SortedList<uint, MemberDescriptor>();
            descriptor.BeforeSerializeMethods = new List<MethodInfo>();
            descriptor.AfterSerializeMethods = new List<MethodInfo>();
            descriptor.BeforeDeserializeMethods = new List<MethodInfo>();
            descriptor.AfterDeserializeMethods = new List<MethodInfo>();
            var orders = new HashSet<uint>();

            var members = typeInfo.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
            foreach (var member in members)
            {
                var propertyInfo = member as PropertyInfo;
                var fieldInfo = member as FieldInfo;
                var methodInfo = member as MethodInfo;

                // We only look for properties, fields and methods
                if (propertyInfo == null && fieldInfo == null && methodInfo == null)
                    continue;

                if (member.GetCustomAttribute<CompilerGeneratedAttribute>() != null)
                    continue;

                // Register before/after serialization hooks
                if (methodInfo != null)
                {
                    var parameterTypes = methodInfo.GetParameters().Select(x => x.ParameterType).ToArray();
                    var beforeSerializeAttribute = member.GetCustomAttribute<BlubBeforeSerializeAttribute>();
                    if (beforeSerializeAttribute != null)
                    {
                        if (methodInfo.ReturnType != typeof(bool) ||
                            !parameterTypes.SequenceEqual(new[] { typeof(BlubSerializer), typeof(BinaryWriter), typeof(string) }))
                        {
                            throw new SerializerException(
                                $"Method '{typeInfo.FullName}.{methodInfo.Name}' marked with {nameof(BlubBeforeSerializeAttribute)} requires the signature bool ({nameof(BlubSerializer)}, BinaryWriter, string)"
                            );
                        }

                        descriptor.BeforeSerializeMethods.Add(methodInfo);
                    }

                    var afterSerializeAttribute = member.GetCustomAttribute<BlubAfterSerializeAttribute>();
                    if (afterSerializeAttribute != null)
                    {
                        if (methodInfo.ReturnType != typeof(void) ||
                            !parameterTypes.SequenceEqual(new[] { typeof(BlubSerializer), typeof(BinaryWriter), typeof(string) }))
                        {
                            throw new SerializerException(
                                $"Method '{typeInfo.FullName}.{methodInfo.Name}' marked with {nameof(BlubAfterSerializeAttribute)} requires the signature void ({nameof(BlubSerializer)}, BinaryWriter, string)"
                            );
                        }

                        descriptor.AfterSerializeMethods.Add(methodInfo);
                    }

                    var beforeDeserializeAttribute = member.GetCustomAttribute<BlubBeforeDeserializeAttribute>();
                    if (beforeDeserializeAttribute != null)
                    {
                        if (methodInfo.ReturnType != typeof(bool) ||
                            !parameterTypes.SequenceEqual(new[] { typeof(BlubSerializer), typeof(BinaryReader), typeof(string) }))
                        {
                            throw new SerializerException(
                                $"Method '{typeInfo.FullName}.{methodInfo.Name}' marked with {nameof(BlubBeforeDeserializeAttribute)} requires the signature bool ({nameof(BlubSerializer)}, BinaryReader, string)"
                            );
                        }

                        descriptor.BeforeDeserializeMethods.Add(methodInfo);
                    }

                    var afterDeserializeAttribute = member.GetCustomAttribute<BlubAfterDeserializeAttribute>();
                    if (afterDeserializeAttribute != null)
                    {
                        if (methodInfo.ReturnType != typeof(void) ||
                            !parameterTypes.SequenceEqual(new[] { typeof(BlubSerializer), typeof(BinaryReader), typeof(string) }))
                        {
                            throw new SerializerException(
                                $"Method '{typeInfo.FullName}.{methodInfo.Name}' marked with {nameof(BlubAfterDeserializeAttribute)} requires the signature void ({nameof(BlubSerializer)}, BinaryReader, string)"
                            );
                        }

                        descriptor.AfterDeserializeMethods.Add(methodInfo);
                    }

                    continue;
                }

                var ignoreAttribute = member.GetCustomAttribute<BlubIgnoreAttribute>();
                if (ignoreAttribute != null)
                    continue;

                var memberAttribute = member.GetCustomAttribute<BlubMemberAttribute>();

                // If type is marked with BlubContractAttribute only include members with BlubMemberAttribute
                if (contractAttribute != null && memberAttribute == null)
                    continue;

                if (propertyInfo?.PropertyType == member.DeclaringType || fieldInfo?.FieldType == member.DeclaringType)
                    throw new SerializerException($"Member '{typeInfo.FullName}.{member.Name}' can not have the declaring type");

                if (propertyInfo != null && (!propertyInfo.CanWrite || !propertyInfo.CanRead))
                    throw new SerializerException($"Property '{typeInfo.FullName}.{member.Name}' needs a getter and setter");

                if (memberAttribute?.Order == null && orders.Count > 0)
                    throw new SerializerException($"Member '{typeInfo.FullName}.{member.Name}' has not a unique order assigned");

                if (memberAttribute?.Order != null && !orders.Add(memberAttribute.Order.Value))
                    throw new SerializerException($"Member '{typeInfo.FullName}.{member.Name}' has not a unique order assigned");

                descriptor.Members.Add(memberAttribute?.Order ?? (uint)descriptor.Members.Count, CreateDescriptorFromMember(member));
            }

            return descriptor;
        }

        private MemberDescriptor CreateDescriptorFromMember(MemberInfo member)
        {
            var fullMemberName = $"{member.DeclaringType.FullName}.{member.Name}";

            MemberDescriptor descriptor = null;
            Type memberType = null;
            switch (member)
            {
                case PropertyInfo propertyInfo:
                    memberType = propertyInfo.PropertyType;
                    descriptor = new PropertyDescriptor
                    {
                        Name = member.Name,
                        Type = memberType,
                        PropertyInfo = propertyInfo
                    };
                    break;

                case FieldInfo fieldInfo:
                    memberType = fieldInfo.FieldType;
                    descriptor = new FieldDescriptor
                    {
                        Name = member.Name,
                        Type = memberType,
                        FieldInfo = fieldInfo
                    };
                    break;

                default:
                    throw new SerializerException(
                        $"Member '{fullMemberName}' is using an unsupported member. Only fields and properties are supported."
                    );
            }

            var serializerAttribute = member.GetCustomAttribute<BlubSerializerAttribute>();
            if (serializerAttribute != null)
            {
                // Serializer was provided by the user

                if (typeof(ISerializer).IsAssignableFrom(serializerAttribute.SerializerType))
                {
                    descriptor.Serializer = (ISerializer)Activator.CreateInstance(serializerAttribute.SerializerType,
                        serializerAttribute.SerializerParameters);

                    if (!descriptor.Serializer.CanHandle(memberType))
                    {
                        throw new SerializerException(
                            $"Serializer '{serializerAttribute.SerializerType.FullName}' on member '{fullMemberName}' can not handle '{memberType.FullName}'"
                        );
                    }
                }
                else if (typeof(ISerializerCompiler).IsAssignableFrom(serializerAttribute.SerializerType))
                {
                    descriptor.Compiler = (ISerializerCompiler)Activator.CreateInstance(serializerAttribute.SerializerType,
                        serializerAttribute.SerializerParameters);

                    if (!descriptor.Compiler.CanHandle(memberType))
                    {
                        throw new SerializerException(
                            $"Serializer '{serializerAttribute.SerializerType.FullName}' on member '{fullMemberName}' can not handle '{memberType.FullName}'"
                        );
                    }
                }
                else
                {
                    throw new SerializerException(
                        $"Serializer '{serializerAttribute.SerializerType.FullName}' assigned to '{fullMemberName}' has to implement {nameof(ISerializer)} or {nameof(ISerializerCompiler)}"
                    );
                }
            }
            else
            {
                // No serializer was provided
                // Check if there are serializers registered or create one if possible

                descriptor.Serializer = _serializer.GetOrCreateSerializer(memberType);
                if (descriptor.Serializer == null)
                    throw new SerializerException($"No serializer available for {fullMemberName}");
            }

            return descriptor;
        }
    }

    internal class Descriptor
    {
        private readonly object _mutex = new object();
        private Queue<Descriptor> _stack;

        public Type Type { get; set; }
        public Descriptor Parent { get; set; }
        public SortedList<uint, MemberDescriptor> Members { get; set; }

        public IList<MethodInfo> BeforeSerializeMethods { get; set; }
        public IList<MethodInfo> AfterSerializeMethods { get; set; }
        public IList<MethodInfo> BeforeDeserializeMethods { get; set; }
        public IList<MethodInfo> AfterDeserializeMethods { get; set; }

        public ISerializer Serializer { get; set; }
        public ISerializerCompiler Compiler { get; set; }

        /// <summary>
        /// Provides the whole type hierarchy from base to upper
        /// </summary>
        /// <returns>Enumerable type hierarchy</returns>
        public IEnumerable<Descriptor> GetTree()
        {
            lock (_mutex)
            {
                if (_stack == null)
                {
                    _stack = new Queue<Descriptor>();
                    AddRecursive(this);
                }

                return _stack;
            }
        }

        private void AddRecursive(Descriptor descriptor)
        {
            if (descriptor.Parent != null)
                AddRecursive(descriptor.Parent);

            _stack.Enqueue(descriptor);
        }
    }

    internal class MemberDescriptor
    {
        public string Name { get; set; }
        public Type Type { get; set; }
        public ISerializer Serializer { get; set; }
        public ISerializerCompiler Compiler { get; set; }
    }

    internal class PropertyDescriptor : MemberDescriptor
    {
        public PropertyInfo PropertyInfo { get; set; }
    }

    internal class FieldDescriptor : MemberDescriptor
    {
        public FieldInfo FieldInfo { get; set; }
    }
}
