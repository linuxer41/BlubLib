﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using BlubLib.Reflection;
using Sigil;
using Sigil.NonGeneric;

namespace BlubLib.Serialization
{
    internal class SerializerGenerator
    {
        private static readonly MethodInfo s_getTypeFromHandleMethod = ReflectionHelper.GetMethod(() => Type.GetTypeFromHandle(default(RuntimeTypeHandle)));

        private readonly BlubSerializer _serializer;
        private readonly Descriptor _descriptor;
        private TypeBuilder _typeBuilder;

        public IList<GeneratedField> Fields { get; }

        public SerializerGenerator(BlubSerializer serializer, Descriptor descriptor)
        {
            if (serializer == null)
                throw new ArgumentNullException(nameof(serializer));

            if (descriptor == null)
                throw new ArgumentNullException(nameof(descriptor));

            _serializer = serializer;
            _descriptor = descriptor;
            Fields = new List<GeneratedField>();
        }

        public ISerializer Generate()
        {
            if (_descriptor.Serializer != null)
                return _descriptor.Serializer;

            _typeBuilder = TypeBuilderFactory.Create(_descriptor.Type.FullName);
            _typeBuilder.AddInterfaceImplementation(typeof(ISerializer<>).MakeGenericType(_descriptor.Type));

            GenerateSerialize(_typeBuilder);
            GenerateDeserialize(_typeBuilder);
            GenerateCanHandle(_typeBuilder);
            GenerateConstructor(_typeBuilder);

            var serializerType = _typeBuilder.CreateTypeInfo();
            var parameters = Fields.Select(f => (object)f.Serializer).ToArray();
            return (ISerializer)Activator.CreateInstance(serializerType, parameters);
        }

        public GeneratedField CreateField(ISerializer serializer)
        {
            var field = Fields.FirstOrDefault(x => x.Serializer == serializer);
            if (field == null)
            {
                var fieldBuilder = _typeBuilder.DefineField($"_{Guid.NewGuid():N}", serializer.GetType(),
                    FieldAttributes.Private | FieldAttributes.Static);
                field = new GeneratedField(fieldBuilder, serializer);
                Fields.Add(field);
            }

            return field;
        }

        private void GenerateConstructor(TypeBuilder typeBuilder)
        {
            var parameters = Fields.Select(f => f.Serializer.GetType()).ToArray();
            var emiter = Emit.BuildConstructor(
                parameters,
                typeBuilder,
                MethodAttributes.Public,
                allowUnverifiableCode: _serializer.AllowUnverifiableCIL,
                doVerify: _serializer.VerifyCIL
            );

            for (var i = 0; i < Fields.Count; i++)
            {
                var field = Fields[i];

                emiter.LoadArgument((ushort)(i + 1));
                emiter.StoreField(field.FieldBuilder);
            }

            emiter.Return();
            emiter.CreateConstructor();
        }

        private void GenerateSerialize(TypeBuilder typeBuilder)
        {
            var emiter = Emit.BuildInstanceMethod(typeof(void),
                new[] { typeof(BlubSerializer), typeof(BinaryWriter), _descriptor.Type },
                typeBuilder,
                nameof(ISerializer<object>.Serialize),
                MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.NewSlot,
                allowUnverifiableCode: _serializer.AllowUnverifiableCIL,
                doVerify: _serializer.VerifyCIL
            );
            var context = new CompilerContext(_serializer, emiter, this);

            using (var objectToSerialize = emiter.DeclareLocal(_descriptor.Type, "objectToSerialize"))
            {
                emiter.LoadValueParam();
                emiter.StoreLocal(objectToSerialize);

                if (_descriptor.Compiler != null)
                {
                    _descriptor.Compiler.EmitSerialize(context, objectToSerialize);
                }
                else if (_descriptor.Serializer != null)
                {
                    context.EmitSerialize(_descriptor.Serializer, objectToSerialize);
                }
                else
                {
                    foreach (var descriptor in _descriptor.GetTree())
                    {
                        foreach (var member in descriptor.Members.Values)
                        {
                            var memberType = member.Type;
                            var skipPropertyLabel = emiter.DefineLabel();
                            foreach (var hookMethod in descriptor.BeforeSerializeMethods)
                            {
                                if (objectToSerialize.LocalType.IsValueType)
                                    emiter.LoadLocalAddress(objectToSerialize);
                                else
                                    emiter.LoadLocal(objectToSerialize);

                                emiter.LoadBlubSerializer();
                                emiter.LoadReaderOrWriterParam();
                                emiter.LoadConstant(member.Name);
                                emiter.Call(hookMethod);
                                emiter.BranchIfTrue(skipPropertyLabel);
                            }

                            using (var value = emiter.DeclareLocal(memberType, "value"))
                            {
                                if (objectToSerialize.LocalType.IsValueType)
                                    emiter.LoadLocalAddress(objectToSerialize);
                                else
                                    emiter.LoadLocal(objectToSerialize);

                                switch (member)
                                {
                                    case PropertyDescriptor property:
                                        emiter.Call(property.PropertyInfo.GetMethod);
                                        break;

                                    case FieldDescriptor field:
                                        emiter.LoadField(field.FieldInfo);
                                        break;
                                }

                                emiter.StoreLocal(value);

                                if (member.Serializer != null)
                                    context.EmitSerialize(member.Serializer, value);
                                else if (member.Compiler != null)
                                    member.Compiler.EmitSerialize(context, value);
                                else
                                    Debug.Assert(false, "Member has no serializer or compiler");
                            }

                            foreach (var hookMethod in descriptor.AfterSerializeMethods)
                            {
                                if (objectToSerialize.LocalType.IsValueType)
                                    emiter.LoadLocalAddress(objectToSerialize);
                                else
                                    emiter.LoadLocal(objectToSerialize);

                                emiter.LoadBlubSerializer();
                                emiter.LoadReaderOrWriterParam();
                                emiter.LoadConstant(member.Name);
                                emiter.Call(hookMethod);
                            }

                            emiter.MarkLabel(skipPropertyLabel);
                        }
                    }
                }
            }

            emiter.Return();

            var methodBuilder = emiter.CreateMethod();
            var genericType = typeof(ISerializer<>).MakeGenericType(_descriptor.Type);
            typeBuilder.DefineMethodOverride(methodBuilder, genericType.GetMethod(nameof(ISerializer<object>.Serialize)));
        }

        private void GenerateDeserialize(TypeBuilder typeBuilder)
        {
            var emiter = Emit.BuildInstanceMethod(_descriptor.Type,
                new[] { typeof(BlubSerializer), typeof(BinaryReader) },
                typeBuilder,
                nameof(ISerializer<object>.Deserialize),
                MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.NewSlot,
                allowUnverifiableCode: _serializer.AllowUnverifiableCIL,
                doVerify: _serializer.VerifyCIL
            );
            var context = new CompilerContext(_serializer, emiter, this);

            using (var objectToDeserialize = emiter.DeclareLocal(_descriptor.Type, "objectToDeserialize"))
            {
                if (_descriptor.Compiler != null)
                {
                    _descriptor.Compiler.EmitDeserialize(context, objectToDeserialize);
                }
                else if (_descriptor.Serializer != null)
                {
                    context.EmitDeserialize(_descriptor.Serializer, objectToDeserialize);
                }
                else
                {
                    // No serializer provided

                    // Initialize object
                    if (_descriptor.Type.IsValueType)
                    {
                        emiter.LoadLocalAddress(objectToDeserialize);
                        emiter.InitializeObject(_descriptor.Type);
                    }
                    else
                    {
                        emiter.NewObject(_descriptor.Type);
                        emiter.StoreLocal(objectToDeserialize);
                    }

                    // Deserialize members
                    foreach (var descriptor in _descriptor.GetTree())
                    {
                        foreach (var member in descriptor.Members.Values)
                        {
                            var memberType = member.Type;
                            var skipPropertyLabel = emiter.DefineLabel();
                            foreach (var hookMethod in descriptor.BeforeDeserializeMethods)
                            {
                                if (objectToDeserialize.LocalType.IsValueType)
                                    emiter.LoadLocalAddress(objectToDeserialize);
                                else
                                    emiter.LoadLocal(objectToDeserialize);

                                emiter.LoadBlubSerializer();
                                emiter.LoadReaderOrWriterParam();
                                emiter.LoadConstant(member.Name);
                                emiter.Call(hookMethod);
                                emiter.BranchIfTrue(skipPropertyLabel);
                            }

                            using (var value = emiter.DeclareLocal(memberType, "value"))
                            {
                                if (member.Serializer != null)
                                    context.EmitDeserialize(member.Serializer, value);
                                else if (member.Compiler != null)
                                    member.Compiler.EmitDeserialize(context, value);
                                else
                                    Debug.Assert(false, "Member has no serializer or compiler");

                                if (objectToDeserialize.LocalType.IsValueType)
                                    emiter.LoadLocalAddress(objectToDeserialize);
                                else
                                    emiter.LoadLocal(objectToDeserialize);

                                emiter.LoadLocal(value);
                                switch (member)
                                {
                                    case PropertyDescriptor property:
                                        emiter.Call(property.PropertyInfo.SetMethod);
                                        break;

                                    case FieldDescriptor field:
                                        emiter.StoreField(field.FieldInfo);
                                        break;
                                }
                            }

                            foreach (var hookMethod in descriptor.AfterDeserializeMethods)
                            {
                                if (objectToDeserialize.LocalType.IsValueType)
                                    emiter.LoadLocalAddress(objectToDeserialize);
                                else
                                    emiter.LoadLocal(objectToDeserialize);

                                emiter.LoadBlubSerializer();
                                emiter.LoadReaderOrWriterParam();
                                emiter.LoadConstant(member.Name);
                                emiter.Call(hookMethod);
                            }

                            emiter.MarkLabel(skipPropertyLabel);
                        }
                    }
                }

                emiter.LoadLocal(objectToDeserialize);
                emiter.Return();
            }

            var methodBuilder = emiter.CreateMethod();
            var genericType = typeof(ISerializer<>).MakeGenericType(_descriptor.Type);
            typeBuilder.DefineMethodOverride(methodBuilder, genericType.GetMethod(nameof(ISerializer<object>.Deserialize)));
        }

        private void GenerateCanHandle(TypeBuilder typeBuilder)
        {
            var emiter = Emit<Func<Type, bool>>.BuildInstanceMethod(
                typeBuilder,
                nameof(ISerializer.CanHandle),
                MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.NewSlot,
                allowUnverifiableCode: _serializer.AllowUnverifiableCIL,
                doVerify: _serializer.VerifyCIL
            );

            emiter.LoadArgument(1);
            emiter.LoadConstant(_descriptor.Type);
            emiter.Call(s_getTypeFromHandleMethod);
            emiter.CompareEqual();
            emiter.Return();

            var methodBuilder = emiter.CreateMethod();
            typeBuilder.DefineMethodOverride(methodBuilder, typeof(ISerializer).GetMethod(nameof(ISerializer.CanHandle)));
        }
    }

    internal class GeneratedField
    {
        public FieldBuilder FieldBuilder { get; }
        public ISerializer Serializer { get; }

        public GeneratedField(FieldBuilder fieldBuilder, ISerializer serializer)
        {
            FieldBuilder = fieldBuilder;
            Serializer = serializer;
        }
    }
}
