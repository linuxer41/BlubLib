using System;
using Sigil;
using Sigil.NonGeneric;

namespace BlubLib.Serialization
{
    public static class EmitExtensions
    {
        /// <summary>
        /// Pushes the BlubSerializer parameter of the Serialize or Deserialize method to the stack
        /// </summary>
        /// <param name="this">The IL emiter</param>
        public static void LoadBlubSerializer(this Emit @this)
        {
            @this.LoadArgument(1);
        }

        /// <summary>
        /// Pushes the BinaryReader or BinaryWriter parameter of the Serialize or Deserialize method to the stack
        /// </summary>
        /// <param name="this">The IL emiter</param>
        public static void LoadReaderOrWriterParam(this Emit @this)
        {
            @this.LoadArgument(2);
        }

        /// <summary>
        /// Pushes the value parameter of the Serialize or Deserialize method to the stack
        /// </summary>
        /// <param name="this">The IL emiter</param>
        internal static void LoadValueParam(this Emit @this)
        {
            @this.LoadArgument(3);
        }

        /// <summary>
        /// Emits IL code to serialize the given object or value
        /// </summary>
        /// <param name="this">The IL emiter</param>
        /// <param name="serializer">The serializer to use for serialization</param>
        /// <param name="value">The value to serialize</param>
        public static void EmitSerialize(this CompilerContext @this, ISerializer serializer, Local value)
        {
            if (serializer == null)
                throw new ArgumentNullException(nameof(serializer));

            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var field = @this.Generator.CreateField(serializer);
            @this.Emit.LoadField(field.FieldBuilder);
            @this.Emit.LoadBlubSerializer();
            @this.Emit.LoadReaderOrWriterParam();
            @this.Emit.LoadLocal(value);
            @this.Emit.CallVirtual(serializer.GetType().GetMethod(nameof(ISerializer<object>.Serialize)));
        }

        /// <summary>
        /// Emits IL code to serialize the given object or value
        /// </summary>
        /// <param name="this">The IL emiter</param>
        /// <param name="value">The value to serialize</param>
        public static void EmitSerialize(this CompilerContext @this, Local value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var serializer = @this.BlubSerializer.GetOrCreateSerializer(value.LocalType);
            if (serializer == null)
                throw new ArgumentException($"No serializer for {value.LocalType.FullName} available", nameof(value.LocalType));

            EmitSerialize(@this, serializer, value);
        }

        /// <summary>
        /// Emits IL code to deserialize the given object or value
        /// </summary>
        /// <param name="this">The IL emiter</param>
        /// <param name="serializer">The serializer to use for deserialization</param>
        /// <param name="value">The value to deserialize</param>
        public static void EmitDeserialize(this CompilerContext @this, ISerializer serializer, Local value)
        {
            if (serializer == null)
                throw new ArgumentNullException(nameof(serializer));

            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var field = @this.Generator.CreateField(serializer);
            @this.Emit.LoadField(field.FieldBuilder);
            @this.Emit.LoadBlubSerializer();
            @this.Emit.LoadReaderOrWriterParam();
            @this.Emit.CallVirtual(serializer.GetType().GetMethod(nameof(ISerializer<object>.Deserialize)));
            @this.Emit.StoreLocal(value);
        }

        /// <summary>
        /// Emits IL code to deserialize the given object or value
        /// </summary>
        /// <param name="this">The IL emiter</param>
        /// <param name="value">The value to deserialize</param>
        public static void EmitDeserialize(this CompilerContext @this, Local value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var serializer = @this.BlubSerializer.GetOrCreateSerializer(value.LocalType);
            if (serializer == null)
                throw new ArgumentException($"No serializer for {value.LocalType.FullName} available", nameof(value.LocalType));

            EmitDeserialize(@this, serializer, value);
        }
    }
}
