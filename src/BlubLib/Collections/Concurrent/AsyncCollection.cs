﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BlubLib.Threading.Tasks;

namespace BlubLib.Collections.Concurrent
{
    public class AsyncCollection<T> : IProducerConsumerCollection<T>
    {
        private readonly IProducerConsumerCollection<T> _items;
        private readonly object _mutex = new object();
        private readonly AwaiterQueue<T> _awaiterQueue = new AwaiterQueue<T>();

        public int Count => _items.Count;

        public bool IsEmpty => Count == 0;

        object ICollection.SyncRoot => throw new NotSupportedException();

        bool ICollection.IsSynchronized => false;

        public AsyncCollection()
            : this(new ConcurrentQueue<T>())
        {
        }

        public AsyncCollection(IProducerConsumerCollection<T> items)
        {
            _items = items;
        }

        public void Add(T item)
        {
            var spin = new SpinWait();
            while (!TryAdd(item))
                spin.SpinOnce();
        }

        public bool TryAdd(T item)
        {
            lock (_mutex)
            {
                // We have no awaiters, try adding the item
                if (_awaiterQueue.IsEmpty)
                    return _items.TryAdd(item);

                // We have atleast one awaiter
                return _awaiterQueue.CompleteOne(item);
            }
        }

        public T Take()
        {
            return Take(CancellationToken.None);
        }

        public T Take(CancellationToken cancellationToken)
        {
            return TakeAsync(cancellationToken).WaitEx();
        }

        public Task<T> TakeAsync()
        {
            return TakeAsync(CancellationToken.None);
        }

        public Task<T> TakeAsync(CancellationToken cancellationToken)
        {
            lock (_mutex)
            {
                if (_items.Count > 0)
                {
                    // We have atleast one item
                    var spin = new SpinWait();
                    T item;
                    while (!_items.TryTake(out item))
                        spin.SpinOnce();
                    return Task.FromResult(item);
                }

                return _awaiterQueue.Enqueue(_mutex, cancellationToken);
            }
        }

        public bool TryTake(out T item)
        {
            lock (_mutex)
            {
                if (_items.Count > 0)
                {
                    var spin = new SpinWait();
                    while (!_items.TryTake(out item))
                        spin.SpinOnce();
                    return true;
                }

                item = default(T);
                return false;
            }
        }

        public void CopyTo(Array array, int index)
        {
            _items.CopyTo(array, index);
        }

        public void CopyTo(T[] array, int index)
        {
            _items.CopyTo(array, index);
        }

        public T[] ToArray()
        {
            return _items.ToArray();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
